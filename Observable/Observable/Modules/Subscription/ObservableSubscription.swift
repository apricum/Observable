//
//  Observable
//
//  Created by August Saint Freytag on 20/01/2021.
//  Copyright © 2021 Rights reserved by project maintainers.
//

public class ObservableSubscription {
	
	public typealias Handler = () -> Void
	
	public let id: UUID = UUID()
	public let handler: Handler
	
	public init(_ handler: @escaping Handler) {
		self.handler = handler
	}
	
}

extension ObservableSubscription: Equatable {
	
	public static func == (lhs: ObservableSubscription, rhs: ObservableSubscription) -> Bool {
		lhs.id == rhs.id
	}
	
}
