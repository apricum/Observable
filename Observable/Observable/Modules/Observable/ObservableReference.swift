//
//  Observable
//
//  Created by August Saint Freytag on 20/01/2021.
//  Copyright © 2021 Rights reserved by project maintainers.
//

public protocol AnyObservableReference {
	
	var willSet: ObservableSubscriptionCollection { get }
	var didSet: ObservableSubscriptionCollection { get }
	
	var anyBase: Any { get }
	
}

@propertyWrapper
public class ObservableReference<Value> {
	
	public private(set) var willSet = ObservableSubscriptionCollection()
	public private(set) var didSet = ObservableSubscriptionCollection()
	
	public var base: Value {
		willSet {
			willSet.notify()
		}
		didSet {
			didSet.notify()
		}
	}
	
	public var wrappedValue: Value {
		get { base }
		set { base = newValue }
	}
	
	public init(_ value: Value) {
		self.base = value
	}
	
	convenience public init(wrappedValue: Value) {
		self.init(wrappedValue)
	}
	
}

extension ObservableReference: AnyObservableReference {

	public var anyBase: Any { base }
	
}
