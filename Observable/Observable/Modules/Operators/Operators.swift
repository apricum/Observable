//
//  Observable
//
//  Created by August Saint Freytag on 20/01/2021.
//  Copyright © 2021 Rights reserved by project maintainers.
//

// MARK: Base Value Get

postfix operator ^^

public postfix func ^^<Value>(_ observable: ObservableReference<Value>) -> Value {
	return observable.base
}

// MARK: Base Value Set

infix operator ^^= : AssignmentPrecedence

public func ^^=<Value>(_ observable: ObservableReference<Value>, _ value: Value) {
	observable.base = value
}
