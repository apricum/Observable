//
//  Observable
//
//  Created by August Saint Freytag on 09/02/2021.
//  Copyright © 2021 Rights reserved by project maintainers.
//

import Foundation

public typealias ObservablesSelectionBlock = () -> Void
public typealias ObservablesSelectionHandlerBlock = (_ block: @escaping ObservableSubscription.Handler) -> Void

/// Operate on the contained subscriptions of all given observables.
public func observables(_ observables: [AnyObservableReference]) -> (willSetEach: ObservablesSelectionHandlerBlock, didSetEach: ObservablesSelectionHandlerBlock, removeAllWillSet: ObservablesSelectionBlock, removeAllDidSet: ObservablesSelectionBlock) {
	let willSetAddBlock = { block in
		observables.forEach { observable in observable.willSet.add(block) }
	}
	
	let didSetAddBlock = { block in
		observables.forEach { observable in observable.didSet.add(block) }
	}
	
	let willSetRemoveBlock = {
		observables.forEach { observable in observable.willSet.removeAllSubscriptions() }
	}
	
	let didSetRemoveBlock = {
		observables.forEach { observable in observable.didSet.removeAllSubscriptions() }
	}
	
	return (willSetAddBlock, didSetAddBlock, willSetRemoveBlock, didSetRemoveBlock)
}
