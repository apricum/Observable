//
//  Observable
//
//  Created by August Saint Freytag on 20/01/2021.
//  Copyright © 2021 Rights reserved by project maintainers.
//

public class ObservableSubscriptionCollection {
	
	public typealias Subscription = ObservableSubscription
	public typealias Handler = ObservableSubscription.Handler
	
	private var subscriptions: [Subscription] = []
	
	@discardableResult
	public func add(_ handler: @escaping Handler) -> Subscription {
		let subscription = Subscription(handler)
		subscriptions.append(subscription)
		
		return subscription
	}
	
	public func remove(_ subscription: Subscription) {
		subscriptions.removeAll(where: { existingSubscription in
			existingSubscription === subscription
		})
	}
	
	public func removeAllSubscriptions() {
		subscriptions.removeAll()
	}
	
	func notify() {
		for subscription in subscriptions {
			subscription.handler()
		}
	}
	
}
